#include <ros/ros.h>
#include <std_msgs/String.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "string_pub");
    ros::NodeHandle node("~");

    ros::Publisher pub_string = node.advertise<std_msgs::String>("/string_topic",1);

    std_msgs::String str;
    node.param<std::string>("retazec", str.data, "DEFAULT_DATA");

    while(ros::ok()) {
        pub_string.publish(str);
        ros::Duration(1).sleep();
    }

    return 0;
}
