#include <ros/ros.h>
#include <std_msgs/String.h>

void msg_cb(const std_msgs::StringConstPtr& msg) {
    ROS_INFO_STREAM(msg->data << "ahoj" << 5);
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "string_sub");
    ros::NodeHandle node("~");

    ros::Subscriber sub = node.subscribe("/input", 1, msg_cb);

    ros::spin();

    return 0;
}
